﻿#ifndef EMAIL_H__
#define EMAIL_H__

#define SENDER ("<asudod@163.com>")	//发件人
#define SMTPSERV ("123.125.50.134")		//163邮箱的SMTP发送服务器地址--smtp.163.com
#define EMAILLEN 1024

int SetSMTPServer(int *sockfd, struct sockaddr_in *servaddr);
//发送邮件
int SendEmail(const char *receiver,	const char *fileContent, const char *subject);

#endif

