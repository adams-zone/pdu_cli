﻿#ifndef COMMON_H__
#define COMMON_H__

#define TAG_START "START"
#define TAG_END "END"
#define CONFIG_CMD_SHELL_FILE "/tmp/set_wireless.sh"
typedef struct AP_INFO_t
{
	int tflag;
	int tp1;
	int ch1;
	char ssid1[64];
	int encry1;
	char key1[256];
	char ssid2[64];
	int encry2;
	char key2[256];

	int tp2;
	int ch2;
	char ssid3[64];
	int encry3;
	char key3[256];
	char ssid4[64];
	int encry4;
	char key4[256];
    int config_lock;
}AP_INFO;

struct sta_mac_list{
	char stamac[20];
	struct sta_mac_list *next;
};

extern void replaceFirst(char *str1, char *str2, char *str3);
extern void replace(char *str1, char *str2, char *str3);
extern void substring(char *dest, char *src, int start, int end);
extern char charAt(char *src, int index);
extern int indexOf(char *str1, const char *str2);
extern int lastIndexOf(char *str1,const char *str2);
extern void ltrim(char *str);
extern void rtrim(char *str);
extern void trim(char *str);
int u2g(char *inbuf, size_t inlen, char *outbuf, size_t outlen);
int g2u(char *inbuf, size_t inlen, char *outbuf, size_t outlen);
int getCode(char* str);
void Get_Info(char* Flag, char* Info, char* recvBytes);    //获取单引号里内容

int sendLoginRequest(int client_socket,char* hwid);
void sendGetAPInfoResponse(int client_socket);
void recvAPConfigRequest(char* recv_buffer, AP_INFO* ap_info);
void sendStaListResponse(int client_socket);
void set_wireless_config(AP_INFO ap_info); //修改无线配置
void get_wireless_config(AP_INFO *ap_info); //获得无线配置
struct sta_mac_list *get_wlan_station_mac_list();

#endif

#define OPENWRT_GET_STSTION_MAC "wlanconfig ath0 list sta|grep -v 'ADDR'|awk '{print $1}'" //获得station信息
//-----------------------------------------------------------------------------device 0
#define OPENWRT_GET_TXPOWER "uci get wireless.wifi0.txpower" 				// 获取无线功率
#define OPENWRT_GET_CHANNEL "uci get wireless.wifi0.channel" 				// 获得无线信道
//-----------------------------------------------------------------------------device 1
#define OPENWRT_GET_TXPOWER_ONE "uci get wireless.wifi1.txpower" 			// 获取无线功率--双频2
#define OPENWRT_GET_CHANNEL_ONE "uci get wireless.wifi1.channel" 			// 获得无线信道--双频2
//-----------------------------------------------------------------------------interface 0
#define OPENWRT_GET_SSID "uci get wireless.@wifi-iface[0].ssid" 			// 获得ssid
#define OPENWRT_GET_ENCRYPTION "uci get wireless.@wifi-iface[0].encryption" 		// 获得无线加密方式
#define OPENWRT_GET_ENKEY "uci get wireless.@wifi-iface[0].key" 			// 获得加密密码
#define OPENWRT_GET_MAXLISTEN "uci get wireless.@wifi-iface[0].max_listen_int" 		// 获得最大连接数
//-----------------------------------------------------------------------------interface 1
#define OPENWRT_GET_SSID_ONE "uci get wireless.@wifi-iface[1].ssid"  			// 获取ssid
#define OPENWRT_GET_ENCRYPTION_ONE "uci get wireless.@wifi-iface[1].encryption" 	// 获取无线加密方式
#define OPENWRT_GET_ENKEY_ONE "uci get wireless.@wifi-iface[1].key"  			// 获取加密密码
#define OPENWRT_GET_MAXLISTEN_ONE "uci get wireless.@wifi-iface[1].max_listen_int" 	// 获取最大连接数
//-----------------------------------------------------------------------------interface 2
#define OPENWRT_GET_SSID_TWO "uci get wireless.@wifi-iface[2].ssid"  			// 获取ssid
#define OPENWRT_GET_ENCRYPTION_TWO "uci get wireless.@wifi-iface[2].encryption" 	// 获取无线加密方式 
#define OPENWRT_GET_ENKEY_TWO "uci get wireless.@wifi-iface[2].key"  			// 获取加密密码
#define OPENWRT_GET_MAXLISTEN_TWO "uci get wireless.@wifi-iface[2].max_listen_int" 	// 获取最大连接数
//-----------------------------------------------------------------------------interface 3
#define OPENWRT_GET_SSID_THERE "uci get wireless.@wifi-iface[3].ssid"  			// 获取ssid
#define OPENWRT_GET_ENCRYPTION_THERE "uci get wireless.@wifi-iface[3].encryption" 	// 获取无线加密方式 
#define OPENWRT_GET_ENKEY_THERE "uci get wireless.@wifi-iface[3].key"  			// 获取加密密码
#define OPENWRT_GET_MAXLISTEN_THERE "uci get wireless.@wifi-iface[3].max_listen_int" 	// 获取最大连接数
/********************************************************************************************************************/

//-----------------------------------------------------------------------------device 0
#define OPENWRT_SET_TXPOWER "uci set wireless.wifi0.txpower" 				// 设置无线功率
#define OPENWRT_SET_CHANNEL "uci set wireless.wifi0.channel" 				// 设置无线信道
//----------------------------------------------------------------------------device 1
#define OPENWRT_SET_TXPOWER_ONE "uci set wireless.wifi1.txpower" 			// 设置无线功率--双频2
#define OPENWRT_SET_CHANNEL_ONE "uci set wireless.wifi1.channel" 			// 设置无线信道--双频2
//-----------------------------------------------------------------------------interface 0
#define OPENWRT_SET_SSID "uci set wireless.@wifi-iface[0].ssid" 			// 设置ssid
#define OPENWRT_SET_ENCRYPTION "uci set wireless.@wifi-iface[0].encryption" 		// 设置无线加密方式
#define OPENWRT_SET_ENKEY "uci set wireless.@wifi-iface[0].key" 			// 设置加密密码
#define OPENWRT_SET_MAXLISTEN "uci set wireless.@wifi-iface[0].max_listen_int" 		// 设置最大连接数
//-----------------------------------------------------------------------------interface 1
#define OPENWRT_SET_SSID_ONE "uci set wireless.@wifi-iface[1].ssid"  			// 设置ssid
#define OPENWRT_SET_ENCRYPTION_ONE "uci set wireless.@wifi-iface[1].encryption" 	// 设置无线加密方式
#define OPENWRT_SET_ENKEY_ONE "uci set wireless.@wifi-iface[1].key"  			// 设置加密密码
#define OPENWRT_SET_MAXLISTEN_ONE "uci set wireless.@wifi-iface[1].max_listen_int" 	// 设置最大连接数
//-----------------------------------------------------------------------------interface 2
#define OPENWRT_SET_SSID_TWO "uci set wireless.@wifi-iface[2].ssid"  			// 设置ssid
#define OPENWRT_SET_ENCRYPTION_TWO "uci set wireless.@wifi-iface[2].encryption" 	// 设置无线加密方式 
#define OPENWRT_SET_ENKEY_TWO "uci set wireless.@wifi-iface[2].key"  			// 设置加密密码
#define OPENWRT_SET_MAXLISTEN_TWO "uci set wireless.@wifi-iface[2].max_listen_int" 	// 设置最大连接数
//-----------------------------------------------------------------------------interface 3
#define OPENWRT_SET_SSID_THERE "uci set wireless.@wifi-iface[3].ssid"  			// 设置ssid
#define OPENWRT_SET_ENCRYPTION_THERE "uci set wireless.@wifi-iface[3].encryption" 	// 设置无线加密方式 
#define OPENWRT_SET_ENKEY_THERE "uci set wireless.@wifi-iface[3].key"  			// 设置加密密码
#define OPENWRT_SET_MAXLISTEN_THERE "uci set wireless.@wifi-iface[3].max_listen_int" 	// 设置最大连接数

#define OPENWRT_COMMIT_CONF "uci set wireless.@wifi-iface[0].wds=0 && uci commit wireless && cfgsave"
#define RES_NET "/etc/init.d/network restart" 						// 重启网络服务

#define OPENWRT_ADD_INTERFACE "uci add wireless wifi-iface" //添加interface
#define OPENWRT_SET_DEF_IFACE_ONE "wireless.@wifi-iface[1]=wifi-iface && wireless.@wifi-iface[1].device=wifi1 && wireless.@wifi-iface[1].network=lan && wireless.@wifi-iface[1].mode=ap && wireless.@wifi-iface[1].lockbssid=0 && wireless.@wifi-iface[1].wds=1 && wireless.@wifi-iface[1].ifup=0" 
#define OPENWRT_SET_DEF_IFACE_TWO "wireless.@wifi-iface[2]=wifi-iface && wireless.@wifi-iface[2].device=wifi1 && wireless.@wifi-iface[2].network=lan && wireless.@wifi-iface[2].mode=ap && wireless.@wifi-iface[2].lockbssid=0 && wireless.@wifi-iface[2].wds=1 && wireless.@wifi-iface[2].ifup=0" 
#define OPENWRT_SET_DEF_IFACE_THERE "wireless.@wifi-iface[3]=wifi-iface && wireless.@wifi-iface[3].device=wifi1 && wireless.@wifi-iface[3].network=lan && wireless.@wifi-iface[3].mode=ap && wireless.@wifi-iface[3].lockbssid=0 && wireless.@wifi-iface[3].wds=1 && wireless.@wifi-iface[3].ifup=0" 




